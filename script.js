var NumericValueValidator = function () {

  var NUMBER_REGEX = /^[\d\.]+$/gm;

  var validateNumberFormat = function (value) {
    if (typeof value === 'string') {
      return value.match(NUMBER_REGEX);
    }
  };

  var updateDOMResult = function (value) {
    document.getElementById('result').value = value;
  };

  this.handlerInstance = function (event) {
    var valueToBeValidated = document.getElementById('numberfield').value;
    var result;
    if (validateNumberFormat(valueToBeValidated)) {
      result = 'true';
      updateDOMResult(result);
    } else {
      result = 'false';
      updateDOMResult(result);
      event.preventDefault();
    }
    return false;
  };

};

var numericValueValidator = new NumericValueValidator().handlerInstance;
document.forms[0].addEventListener('submit', numericValueValidator);
